export function requestHeaders() {
    return {'AUTHORIZATION': localStorage.getItem("jwt")}
}

export default requestHeaders;