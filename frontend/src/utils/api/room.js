import {requestHeaders, config, axios} from './index'

class roomApi {
    static index() {
        return axios.get(config.appHost + '/v1/rooms.json', {headers: requestHeaders()})
    }

    static get(id){
        return axios.get(config.appHost + `/v1/rooms/${id}`, {headers: requestHeaders()})
    }

    static delete(id){
        return axios.delete(config.appHost + `/v1/rooms/${id}`, {headers: requestHeaders()})
    }

    static create(data){
        return axios.post(config.appHost + `/v1/rooms`, data, {headers: requestHeaders()})
    }

    static close(id){
        return axios.post(config.appHost + `/v1/rooms/${id}/close`, {},{headers: requestHeaders()})
    }

}

export default roomApi;