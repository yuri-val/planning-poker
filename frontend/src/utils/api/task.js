import {requestHeaders, config, axios} from './index'

class taskApi {
    static get(id){
        return axios.get(config.appHost + `/v1/tasks/${id}`, {headers: requestHeaders()})
    }

}

export default taskApi;