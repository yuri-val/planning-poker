import {requestHeaders, config, axios} from './index'

class authApi{
    static login_check(){
        return axios.get(config.appHost + '/login_check', {headers: requestHeaders()})
    }
}

export default authApi;