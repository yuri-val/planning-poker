import {requestHeaders, config, axios} from './index'

class markApi{
    static allowed(){
        return axios.get(config.appHost + '/v1/users_marks/allowed', {headers: requestHeaders()})
    }

    static set(data){
        return axios.post(config.appHost + '/v1/users_marks/set', data, {headers: requestHeaders()})
    }
}

export default markApi;