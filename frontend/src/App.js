import React, { Component } from 'react';
import './App.css';
import LoginForm      from './components/LoginForm';
import RoomsContainer from './components/Rooms/RoomsContainer';
import queryString from "query-string";
import { Header, NavBar } from "./components/Layout/index";
import authApi from "./utils/api/auth";


class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: ' ',
            password: ' ',
            isLogged: false,
            room: ''
        };
    }

    setCurrentRoom = (e) => {
        const parsed = queryString.parse(window.location.search)
        this.setState({room: parsed.room})
    }

    componentDidMount() {
        this.setCurrentRoom();
        authApi.login_check()
            .then(response => {
                console.log(response);
                this.setState({isLogged: response.status === 200})
            })
            .catch(error => console.log(error))
    }

    logout = (e) =>  {
        e.preventDefault();
        localStorage.setItem("jwt", '')
        window.location.reload()
    }

    render() {
        const isLogged = this.state.isLogged;
        const room = this.state.room;
        console.log(room);
        return (
          <div className="App">
              { isLogged ? (
                  <NavBar logoutCallback = {this.logout}/>
              ) : (
                  <br />
              )
              }
            <Header/>
              { !isLogged ? (
                <LoginForm/>
              ) : (
                <RoomsContainer appComponent={this}/>
              )}
          </div>
    );
  }
}

export default App;
