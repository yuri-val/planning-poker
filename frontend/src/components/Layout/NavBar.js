import React, { Component } from 'react';
import { Button, Menu } from 'antd';

class NavBar extends Component {

    render() {
        return (
            <Menu mode="horizontal" align="right">
                <Menu.Item>
                    <Button icon="logout" type="danger" onClick={this.props.logoutCallback}>
                        Logout
                    </Button>
                </Menu.Item>
            </Menu>
        )
    };
}

export default NavBar;