import React, { Component } from 'react'
import { Button, Modal, Radio, Row, Col } from 'antd'
import roomApi from "../../utils/api/room";
import markApi from "../../utils/api/mark";

class ShowRoom extends Component {

    constructor(props) {
        super(props)
        this.state = {
            room: {},
            isAdmin: false,
            rComp: null,
            task: {},
            all_marks: []
        }

    }

    closeRoom(id){
        const confirm = Modal.confirm;

        confirm({
            title: 'Are you sure close this room?',
            content: '',
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk: function () {
                roomApi.close(id)
                    .then(response => {
                        console.log(response);
                        this.state.rComp.back();
                    })
                    .catch(error => console.log(error))
            },
            onCancel() {
                console.log('Cancel');
            },
        });
    }

    componentDidMount() {
        markApi.allowed()
            .then(response => {
                console.log(response)
                this.setState({all_marks: response.data})
            })
            .catch(error => console.log(error));

    }

    adminBar(room){
        return <div align="center">
                    <Button type="primary" icon="left-circle-o" onClick={this.props.rComp.back}>
                        Back
                    </Button>
                    <Button id={room.id} type="danger" icon="close-circle-o" onClick={e => this.closeRoom(e.target.id)}>
                        Close room
                    </Button>
                    <Button icon="forward" onClick={this.back}>
                        Next task
                    </Button>
                </div>

    }

    setMark(task_id, mark){
        let data = {
            task_id: task_id,
            mark: mark
        };
        console.log(data)
        markApi.set(data)
            .then(response => {
                console.log(response)
            })
            .catch(error => console.log(error));
    }

    render() {

        console.log(this.props)
        let room = this.props.room;
        let isAdmin = room.is_admin;

        return(
            <div align="center">
                <Row>
                    <Col span={18}>
                        {isAdmin ? (this.adminBar(room)) : (<br/>)}
                        <h1>{room.name}</h1>
                        <br/>
                        <br/>
                        <h3>Current task: <strong>{room.current_task_desc}</strong></h3>
                        <br/>
                        <h2>Set your mark:</h2>
                        <Radio.Group defaultValue={this.state.all_marks[0]}>
                            {this.state.all_marks.map((mark) => {
                                return(
                                    <Radio.Button key={mark} value={mark} onClick={e => this.setMark(room.current_task_id, this.props.value)}>{mark}</Radio.Button>
                                )
                            })}
                        </Radio.Group>
                    </Col>
                    <Col>
                        Hello
                    </Col>
                </Row>
            </div>)
    }

}

export default ShowRoom;