import React, { Component } from 'react'
import { Button, Modal, Form, Input, Radio, Icon } from 'antd';

let uuid = 0;

const EditRoom = Form.create()(

    class extends Component {



        remove = (k) => {
            const { form } = this.props;
            // can use data-binding to get
            const keys = form.getFieldValue('keys');
            // We need at least one passenger
            if (keys.length === 1) {
                return;
            }

            // can use data-binding to set
            form.setFieldsValue({
                keys: keys.filter(key => key !== k),
            });
        };

        add = () => {
            const { form } = this.props;
            // can use data-binding to get
            const keys = form.getFieldValue('keys');
            const nextKeys = keys.concat(uuid);
            uuid++;
            // can use data-binding to set
            // important! notify form to detect changes
            form.setFieldsValue({
                keys: nextKeys,
            });
        };


        render() {
            const {visible, onCancel, onCreate, form} = this.props;
            const {getFieldDecorator, getFieldValue} = form;
            //const FormItem = Form.Item;

            //const { getFieldDecorator, getFieldValue } = this.props.form;
            const formItemLayout = {
                labelCol: {
                    xs: { span: 24 },
                    sm: { span: 4 },
                },
                wrapperCol: {
                    xs: { span: 24 },
                    sm: { span: 20 },
                },
            };
            const formItemLayoutWithOutLabel = {
                wrapperCol: {
                    xs: { span: 34, offset: 0 },
                    sm: { span: 30, offset: 4 },
                },
            };
            getFieldDecorator('keys', { initialValue: [] });
            const keys = getFieldValue('keys');
            const formItems = keys.map((k, index) => {
                return (
                    <Form.Item
                        {...(index === 0 ? formItemLayout : formItemLayoutWithOutLabel)}
                        label={index === 0 ? 'Tasks' : ''}
                        required={false}
                        key={k}
                    >
                        {getFieldDecorator(`tasks[${k}]`, {
                            validateTrigger: ['onChange', 'onBlur'],
                            rules: [{
                                required: true,
                                whitespace: true,
                                message: "Please input task's description or delete this field.",
                            }],
                        })(
                            <Input placeholder="task description" style={{ width: '90%', marginRight: 8 }} />
                        )}
                        {keys.length > 1 ? (
                            <Icon
                                className="dynamic-delete-button"
                                type="minus-circle-o"
                                disabled={keys.length === 1}
                                onClick={() => this.remove(k)}
                            />
                        ) : null}
                    </Form.Item>
                );
            });

            return (
                <Modal
                    visible={visible}
                    title="Create a new room"
                    okText="Create"
                    onCancel={onCancel}
                    onOk={onCreate}
                >
                    <Form layout="vertical">
                        <Form.Item label="Name">
                            {getFieldDecorator('name', {
                                rules: [{required: true, message: 'Please input the name of new room!'}],
                            })(
                                <Input/>
                            )}
                        </Form.Item>

                        {formItems}
                        <Form.Item {...formItemLayoutWithOutLabel}>
                            <Button type="dashed" onClick={this.add} style={{ width: '60%' }}>
                                <Icon type="plus" /> Add task
                            </Button>
                        </Form.Item>

                    </Form>
                </Modal>
            );
        }
    });


export default EditRoom;