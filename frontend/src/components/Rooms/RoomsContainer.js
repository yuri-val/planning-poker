import React, { Component } from 'react'
import { Card, Col, Row, Button, Modal, Icon, Tag } from 'antd';
import EditRoom from "./EditRoom";
import roomApi from "../../utils/api/room"
import ShowRoom from "./ShowRoom";


class RoomsContainer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            rooms: [],
            room: {},
            showList: 'block',
            showItem: 'none',
            visible: false,

        }
    }

    showModal = () => {
        this.setState({ visible: true });
    }
    handleCancel = () => {
        this.setState({ visible: false });
    }
    handleCreate = () => {
        const form = this.formRef.props.form;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }

            console.log('Received values of form: ', values);
            roomApi.create(values)
                .then(response => {
                    console.log(response)
                    this.componentDidMount();
                })
                .catch(error => console.log(error))
            form.resetFields();
            this.setState({ visible: false });
        });
    }
    saveFormRef = (formRef) => {
        this.formRef = formRef;
    }

    showDeleteConfirm (component, id){

        const confirm = Modal.confirm;

        confirm({
            title: 'Are you sure delete this room?',
            content: 'All questions and answers will also be deleted!',
            okText: 'Yes',
            okType: 'danger',
            cancelText: 'No',
            onOk: function () {
                roomApi.delete(id)
                    .then(response => {
                        console.log(response)
                        component.componentDidMount();
                    })
                    .catch(error => console.log(error))
            },
            onCancel() {
                console.log('Cancel');
            },
        });
    }

    setCurrentRoom = (e, component, room_id) =>{
        window.history.pushState("", "", '?room=' + room_id);
        this.props.appComponent.setCurrentRoom();
        this.setDisplayRoom(e, 'none', 'block')
        this.open(room_id)
    }

    unsetCurrentRoom = (e) =>{
        window.history.pushState("", "", " ");
    }


    componentDidMount() {
        roomApi.index()
            .then(response => {
                console.log(response)
                this.setState({rooms: response.data})
            })
            .catch(error => console.log(error));
        console.log(this.props.appComponent.state.room)
        if(this.props.appComponent.state.room){
            this.setCurrentRoom(null, this, this.props.appComponent.state.room)
        }
    }

    open = (id) => {
        roomApi.get(id)
            .then(response => {
                console.log(response)
                this.setState({room: response.data})
                this.setState({showList: 'none'})
                this.setState({showItem: 'block'})
            })
            .catch(error => console.log(error))
        //this.setState({room: 'slug'})

    }

    back = (e) => {
        this.setDisplayRoom(e, 'block', 'none')
        this.unsetCurrentRoom(e)
    }

    setDisplayRoom = (e, list, item) =>{
        this.setState({showList: list})
        this.setState({showItem: item})
    }

    render() {
        const gridStyle = {
            width: '25%',
            textAlign: 'center',
        };

        return (
            <div>
                <div id='rooms' style={{padding: '30px', display: this.state.showList }}>
                    <Row gutter={8}>
                        <Col span={6}>
                            <Button type="dashed" onClick={this.showModal}>
                                <Icon type="plus" /> Add new room
                            </Button>
                            <EditRoom
                                wrappedComponentRef={this.saveFormRef}
                                visible={this.state.visible}
                                onCancel={this.handleCancel}
                                onCreate={this.handleCreate}
                            />
                        </Col>
                        {this.state.rooms.map((room) => {
                            return(
                                <Col span={6}  key={room.id}>
                                    <Card key={room.id} title={room.name} style={{ width: 300 }}
                                            extra={<div><Button id={room.slug}
                                                           type="primary"
                                                           shape="circle"
                                                           icon="search"
                                                           onClick={e => this.setCurrentRoom(e, this, e.target.id)}/>
                                                        <Button id={room.slug}
                                                            type="danger"
                                                            shape="circle"
                                                            icon="delete"
                                                            onClick={e => this.showDeleteConfirm(this, e.target.id)}/>
                                                    </div>}>
                                        Tasks: <Tag color="#87d068">{room.tasks_count}</Tag>
                                        Marks: <Tag color="#108ee9">{room.marks_count}</Tag>

                                    </Card>
                                </Col>

                        )
                        })}
                    </Row>
                </div>
                <div id='room' style={{ display: this.state.showItem }}>
                    { this.state.showItem == 'block' ? (
                        <ShowRoom room={this.state.room} rComp={this}/>
                    ) : (
                        <p />
                        )
                    }
                </div>
            </div>
        );
    }
}

export default RoomsContainer