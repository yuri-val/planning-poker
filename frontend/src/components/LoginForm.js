import React, { Component } from 'react';
import axios from 'axios';
import config from "../utils/config";
import { Form, Input, Button, Icon } from 'antd';

class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: ' ',
            password: ' '
        };
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    login = (e) =>  {
        e.preventDefault();
        let body = {
            email: this.state.email,
            password: this.state.password
        }
        console.log(body)
        axios.post(config.appHost + "/login", body)
            .then(response => {
                console.log(response)
                localStorage.setItem("jwt", response.data.auth_token)
                window.location.reload()
            })
            .catch(error => console.log(error))
    }

    signUp = (e) =>  {
        e.preventDefault();
        let body = {
            email: this.state.email,
            password: this.state.password
        }
        console.log(body)
        axios.post(config.appHost + "/sign_up", body)
            .then(response => {
                console.log(response)
                localStorage.setItem("jwt", response.data.auth_token)
                window.location.reload()
            })
            .catch(error => console.log(error))
    }

    render() {
        return (
            <Form className="login-form">
                <Form.Item>
                    <label htmlFor="email">Email: </label>
                    <Input
                        name="email"
                        id="email"
                        type="email"
                        value={this.state.email}
                        onChange={e => this.handleChange(e)}
                    />
                </Form.Item>
                <Form.Item>
                <label htmlFor="password">Password:</label>
                <Input
                    name="password"
                    id="password"
                    type="password"
                    value={this.state.password}
                    onChange={e => this.handleChange(e)}
                />
                </Form.Item>
                <div style={{margin: 'auto'}}>
                    <Button type="primary" onClick={this.login}>
                        <Icon type="left" /> Login
                    </Button>
                    <Button type="default" onClick={this.signUp}>
                        <Icon type="left" /> Sign Up
                    </Button>
                </div>
            </Form>
        )
    }
}

export default LoginForm;