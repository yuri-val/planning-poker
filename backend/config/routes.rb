Rails.application.routes.draw do
  post 'login', to: 'authentication#authenticate'
  get 'login_check', to: 'authentication#login_check'
  post 'sign_up', to: 'authentication#sign_up'
  namespace :v1, defaults: { format: :json } do
    resources :rooms
    resources :tasks

    get 'users_marks/allowed', to: 'users_marks#allowed'
    post 'users_marks/set', to: 'users_marks#set'
    post 'rooms/:id/close', to: 'rooms#close'
  end
end