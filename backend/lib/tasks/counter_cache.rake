desc 'Counter cache for room has many tasks'

task task_counter: :environment do
  Room.reset_column_information
  Room.find_each do |p|
    Room.reset_counters p.id, :tasks
  end
end