module V1
  class RoomsController < ApplicationController

    def create
      begin
        @room = Room.new
        @room.name = params[:name]
        @room.admin = current_user
        if @room.save
          params[:tasks].each do |d_task|
            @task = Task.new
            @task.description = d_task
            @task.room = @room
            @task.save
          end
        end
        render json: {message: 'Ok'}
      rescue
        render json: {error: [@room.errors.full_messages, @task..errors.full_messages].join("\n")}
      end

    end

    def index
      @rooms = Room.by_user(current_user).order(id: :desc)
      render json: @rooms
    end

    def show
      @room = Room.friendly.find(params[:id])
      data = @room.serializable_hash
      data[:is_admin] = current_user == @room.admin
      data[:current_task_desc] = Task.find(@room.current_task_id).description
      render json: data
    end

    def destroy
      @room = Room.friendly.find(params[:id])
      if @room.destroy!
        render json: {message: 'Ok'}
      else
        render json: {error: @room.errors.full_messages}
      end
    end

    def close
      @room = Room.friendly.find(params[:id])
      if @room.close!
        render json: {message: 'Ok'}
      else
        render json: {error: @room.errors.full_messages}
      end

    end

  end
end