module V1
  class UsersMarksController < ApplicationController
    def allowed
      @marks = UsersMark.allowed_marks
      render json: @marks
    end

    def set
      data = {
          user: current_user,
          task_id: params[:task_id]
      }
      @mark = UsersMark.find_or_create_by(data)
      @mark.mark = params[:mark]
      if @mark.save!
        render json: {message: 'Ok'}
      else
        render json: {error: @mark.errors.full_messages}
      end
    end


  end
end