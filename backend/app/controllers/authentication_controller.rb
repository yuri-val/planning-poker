class AuthenticationController < ApplicationController
  skip_before_action :authenticate_request

  def authenticate
    command = AuthenticateUser.call(params[:email], params[:password])

    if command.success?
      render json: { auth_token: command.result }
    else
      render json: { error: command.errors }, status: :unauthorized
    end
  end

  def sign_up

    user = User.find_by_email(params[:email])

    unless user.nil?
      render json: { error: "User with e-mail #{params[:email]} already exist.
                            Please, login." }
      return
    end

    User.create!(email: params[:email],
                 password: params[:password],
                 password_confirmation: params[:password])

    command = AuthenticateUser.call(params[:email], params[:password])

    if command.success?
      render json: { auth_token: command.result }
    else
      render json: { error: command.errors }, status: :unauthorized
    end
  end

  def login_check
    result = authenticate_request
    render json: { message: 'Ok' }, status: 200 if result.nil?
  end


end