class Room < ApplicationRecord
  extend FriendlyId
  friendly_id :fr_name, use: :slugged, sequence_separator: '_'

  belongs_to :admin, class_name: 'User'
  has_many :tasks, dependent: :destroy
  has_many :marks, class_name: 'UsersMark', through: :tasks, source: :users_marks

  scope :by_user, ->(user) { includes(:tasks).where(admin: user) }

  def normalize_friendly_id(text)
    text.to_slug.normalize! :transliterations => [:russian, :latin]
  end

  def update_marks_count!
    self.marks_count = marks.count
    self.save
  end

  def close!
    self.closed = true
    self.save!
  end

  def next_task!
    next_task = self.tasks.where('id > ?', [self.current_task_id]).order(id: :asc).limit(1)
    if next_task.size > 0

    end
  end

  private

  def fr_name
    "#{name}-#{id}"
  end

end
