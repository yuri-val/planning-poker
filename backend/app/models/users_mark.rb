class UsersMark < ApplicationRecord
  belongs_to :user
  belongs_to :task
  has_one :room, through: :task

  after_commit :update_marks_count

  def self.allowed_marks
    [1, 2, 3, 5, 8, 13, 20]
  end

  private

  def update_marks_count
    room.update_marks_count!
  end

end
