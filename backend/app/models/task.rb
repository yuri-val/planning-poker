class Task < ApplicationRecord
  belongs_to :room, counter_cache: true
  has_many :users_marks, dependent: :destroy

  after_commit :update_current_task

  private

  def update_current_task
    if self.room.current_task_id.nil?
      c_room = self.room
      c_room.current_task_id = self.id
      c_room.save
    end
  end


end
