class CreateUsersMarks < ActiveRecord::Migration[5.1]
  def change
    create_table :users_marks do |t|
      t.references :user, foreign_key: true
      t.references :task, foreign_key: true
      t.integer :mark

      t.timestamps
    end
  end
end
