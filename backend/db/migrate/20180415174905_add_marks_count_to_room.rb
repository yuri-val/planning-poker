class AddMarksCountToRoom < ActiveRecord::Migration[5.1]
  def change
    add_column :rooms, :marks_count, :integer, default: 0
  end
end
