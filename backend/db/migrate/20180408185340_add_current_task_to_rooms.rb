class AddCurrentTaskToRooms < ActiveRecord::Migration[5.1]
  def change
    add_reference :rooms, :current_task, foreign_key: { to_table: :tasks }
  end
end
