class CreateTasks < ActiveRecord::Migration[5.1]
  def change
    create_table :tasks do |t|
      t.references :room, foreign_key: true
      t.string :description
      t.boolean :closed

      t.timestamps
    end
  end
end
