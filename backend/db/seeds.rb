# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

10.times do |i|
  User.create(email: "test_user_#{i}@ex.com",
               password: "test_user_pass_#{i}",
               password_confirmation: "test_user_pass_#{i}")
end

us_isd = User.all.ids
statuses = [true, false]
all_marks = UsersMark.allowed_marks

10.times do |i|

  r = Room.create(name: "Test room #{i}",
              closed: statuses.sample,
              admin_id: us_isd.sample
              )

  tasks_count = rand(10)

  tasks_count.times do |t_i|

    t = Task.create(description: "Task ##{t_i}",
                    room: r)

    us_isd.each do |uid|
      if statuses.sample
        um = UsersMark.create(user_id: uid,
                              task: t,
                              mark: all_marks.sample)
      end
    end

    r.current_task = r.tasks.to_a.sample if r.tasks_count

    r.save

  end


end